db.createUser(
    {
        user: "myuser",
        pwd: "test123",
        roles: [
            {
                role: "readWrite",
                db: "spring-boot-reactive-mongo"
            }
        ]
    }
);

db = new Mongo().getDB("spring-boot-reactive-mongo");

exports = async function () {
    const author1Id = ObjectId("507c7f79bcf86cd7994f6c0e");
    const author2Id = ObjectId("507f1f77bcf86cd799439011");

    await db.author.insertMany([
        {
            _id: author1Id,
            authorName: "J.R.R. Tolkien",
        },
        {
            _id: author2Id,
            authorName: "J.K. Rowling",
        },
    ]);

    const author1 = await db.author.findOne({"authorName": "J.R.R. Tolkien"}, {})
    const author2 = await db.author.findOne({"authorName": "J.K. Rowling"}, {})
    // -- Return only '_id' and 'authorId' fields
    // -- @link https://www.mongodb.com/docs/manual/reference/method/db.collection.findOne
    // const author1_id = await db.author.findOne({"authorName": "J.K. Rowling"}, {_id: 1})

    console.log("Author1: ", author1)
    console.log("Author2: ", author2)
    console.log("Author1 object id: ", author1._id)
    console.log("Author1 object id val: ", author1._id.valueOf())

    const book1Id = ObjectId("63ced8e612dbd944feeed746");

    await db.book.insertMany([
        {
            _id: book1Id,
            "title": "The Lord of the Rings - The Fellowship of the Ring",
            "publicationDate": ISODate("1954-07-29"),
            "pages": 800,
            "authors": [
                author1._id.valueOf(),
            ],
        },
        {
            "title": "Harry Potter - Philosopher's Stone",
            "publicationDate": ISODate("1997-06-26"),
            "pages": 250,
            "authors": [
                author2._id.valueOf(),
            ],
        },
        {
            "title": "Harry Potter - Chamber of Secrets",
            "publicationDate": ISODate("1998-07-02"),
            "pages": 216,
            "authors": [
                author2._id.valueOf(),
            ],
        },
    ]);

    console.log(db.book.find().pretty())
    console.log(db.author.find().pretty())

    return "Done!";
};

console.log(exports());

