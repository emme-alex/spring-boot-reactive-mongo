package it.matteo.spring_boot_reactive_mongo;

import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

@Component
public class HomeHandler {

    public Mono<ServerResponse> index(final ServerRequest request) {
        return ServerResponse
                .ok()
                .body(BodyInserters.fromValue("Hello World!!!"));
    }

}
