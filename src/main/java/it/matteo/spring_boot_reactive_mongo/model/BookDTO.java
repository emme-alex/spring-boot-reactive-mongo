package it.matteo.spring_boot_reactive_mongo.model;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
public class BookDTO {

    private String bookId;

    @NotNull
    @Size(max = 255)
    private String title;

    private Long pages;

    private LocalDateTime publicationDate;

    private List<String> authors;

}
