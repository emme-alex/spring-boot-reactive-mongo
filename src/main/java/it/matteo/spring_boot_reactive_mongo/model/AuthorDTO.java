package it.matteo.spring_boot_reactive_mongo.model;

import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class AuthorDTO {

    private String authorId;

    @Size(max = 255)
    private String authorName;

}
