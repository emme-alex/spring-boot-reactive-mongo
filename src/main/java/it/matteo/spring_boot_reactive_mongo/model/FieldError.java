package it.matteo.spring_boot_reactive_mongo.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FieldError {

    private String field;
    private String errorCode;

}
