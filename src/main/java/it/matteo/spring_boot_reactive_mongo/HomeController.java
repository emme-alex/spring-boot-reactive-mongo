package it.matteo.spring_boot_reactive_mongo;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springdoc.core.annotations.RouterOperation;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.reactive.function.server.RequestPredicates;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;

@Configuration
public class HomeController {

    @Bean
    @RouterOperation(
            method = RequestMethod.GET,
            path = "/",
            produces = MediaType.TEXT_PLAIN_VALUE,
            operation = @Operation(
                    method = "get",
                    summary = "No operation, just an 'Hello World'",
                    description = "No operation, just an 'Hello World'",
                    operationId = "index",
                    responses = @ApiResponse(responseCode = "200",
                            content = @Content(
                                    mediaType = MediaType.TEXT_PLAIN_VALUE,
                                    array = @ArraySchema(schema = @Schema(implementation = String.class)))))
    )
    public RouterFunction<ServerResponse> index(final HomeHandler handler) {
        return RouterFunctions.route(
                RequestPredicates.GET("/"), handler::index);
    }

}
