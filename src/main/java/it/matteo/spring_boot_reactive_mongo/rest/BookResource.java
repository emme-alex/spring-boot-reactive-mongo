package it.matteo.spring_boot_reactive_mongo.rest;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import it.matteo.spring_boot_reactive_mongo.model.BookDTO;
import it.matteo.spring_boot_reactive_mongo.service.BookService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.QueryMapping;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;


@RestController
@RequestMapping(value = "/api/books", produces = MediaType.APPLICATION_JSON_VALUE)
@Slf4j
@RequiredArgsConstructor
public class BookResource {

    private final BookService bookService;

    @GetMapping
    @QueryMapping
    @Operation(
            method = "get",
            summary = "Get all books",
            description = "Get info about all books",
            operationId = "getAll",
            responses = @ApiResponse(
                    responseCode = "200",
                    content = @Content(
                            mediaType = MediaType.APPLICATION_JSON_VALUE,
                            array = @ArraySchema(schema = @Schema(implementation = BookDTO.class)))))
    public Flux<BookDTO> getAllBooks() {
        return bookService.findAll();
    }

    @GetMapping("/{bookId}")
    @QueryMapping
    @Operation(
            method = "get",
            summary = "Get a single book",
            description = "Get info about a single book",
            operationId = "get",
            responses = @ApiResponse(
                    responseCode = "200",
                    content = @Content(
                            mediaType = MediaType.APPLICATION_JSON_VALUE,
                            schema = @Schema(implementation = BookDTO.class))))
    public Mono<BookDTO> getBook(@PathVariable @Argument final String bookId) {
        return bookService.get(bookId);
    }

    @PostMapping
    @Operation(
            method = "post",
            summary = "Create a book",
            description = "Create a book",
            operationId = "create",
            requestBody = @io.swagger.v3.oas.annotations.parameters.RequestBody(
                    required = true,
                    description = "Create a book request",
                    content = @Content(
                            mediaType = MediaType.APPLICATION_JSON_VALUE,
                            schema = @Schema(implementation = BookDTO.class))),
            responses = {
                    @ApiResponse(
                            responseCode = "201",
                            content = @Content(
                                    mediaType = MediaType.TEXT_PLAIN_VALUE,
                                    schema = @Schema(implementation = String.class)))
            })
    public Mono<String> createBook(@RequestBody @Valid final BookDTO bookDTO) {
        return bookService.create(bookDTO);
    }

    @PutMapping("/{bookId}")
    @Operation(
            method = "put",
            summary = "Update a book",
            description = "Update a book",
            operationId = "update",
            parameters = @Parameter(
                    name = "bookId",
                    description = "The book id",
                    in = ParameterIn.PATH,
                    required = true),
            requestBody = @io.swagger.v3.oas.annotations.parameters.RequestBody(
                    required = true,
                    description = "Update a book request",
                    content = @Content(
                            mediaType = MediaType.APPLICATION_JSON_VALUE,
                            schema = @Schema(implementation = BookDTO.class))),
            responses = @ApiResponse(responseCode = "200"))
    public Mono<Void> updateBook(
            @PathVariable final String bookId,
            @RequestBody @Valid final BookDTO bookDTO) {

        return bookService.update(bookId, bookDTO);
    }

    @DeleteMapping("/{bookId}")
    @Operation(
            method = "delete",
            summary = "Delete a book",
            description = "Delete a book",
            operationId = "delete",
            parameters = @Parameter(
                    name = "bookId",
                    description = "The book id",
                    in = ParameterIn.PATH,
                    required = true),
            responses = @ApiResponse(responseCode = "204"))
    public Mono<Void> deleteBook(@PathVariable final String bookId) {
        return bookService.delete(bookId);
    }

}
