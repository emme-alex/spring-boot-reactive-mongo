package it.matteo.spring_boot_reactive_mongo.rest;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import it.matteo.spring_boot_reactive_mongo.model.AuthorDTO;
import it.matteo.spring_boot_reactive_mongo.service.AuthorService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.QueryMapping;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;


@RestController
@RequestMapping(value = "/api/authors")
@RequiredArgsConstructor
public class AuthorResource {

    private final AuthorService authorService;

    @GetMapping
    @QueryMapping
    @Operation(
            method = "get",
            summary = "Get all authors",
            description = "Get info about all authors",
            operationId = "getAll",
            responses = @ApiResponse(responseCode = "200",
                    content = @Content(
                            mediaType = MediaType.APPLICATION_JSON_VALUE,
                            array = @ArraySchema(schema = @Schema(implementation = AuthorDTO.class)))))
    public Flux<AuthorDTO> getAllAuthors() {
        return authorService.findAll();
    }

    @GetMapping("/{authorId}")
    @QueryMapping
    @Operation(
            method = "get",
            summary = "Get a single author",
            description = "Get info about a single author",
            operationId = "get",
            responses = @ApiResponse(responseCode = "200",
                    content = @Content(
                            mediaType = MediaType.APPLICATION_JSON_VALUE,
                            schema = @Schema(implementation = AuthorDTO.class))))
    public Mono<AuthorDTO> getAuthor(@PathVariable @Argument final String authorId) {
        return authorService.get(authorId);
    }

    @PostMapping
    @Operation(
            method = "post",
            summary = "Create an author",
            description = "Create an author",
            operationId = "create",
            requestBody = @io.swagger.v3.oas.annotations.parameters.RequestBody(
                    required = true,
                    description = "Create an author request",
                    content = @Content(
                            mediaType = MediaType.APPLICATION_JSON_VALUE,
                            schema = @Schema(implementation = AuthorDTO.class))),
            responses = {
                    @ApiResponse(
                            responseCode = "201",
                            content = @Content(
                                    mediaType = MediaType.TEXT_PLAIN_VALUE,
                                    schema = @Schema(implementation = String.class)))
            })
    public Mono<String> createAuthor(@RequestBody @Valid final AuthorDTO authorDTO) {
        return authorService.create(authorDTO);
    }

    @PutMapping("/{authorId}")
    @Operation(
            method = "put",
            summary = "Update an author",
            description = "Update an author",
            operationId = "update",
            parameters = @Parameter(name = "authorId", description = "The author id", in = ParameterIn.PATH, required = true),
            requestBody = @io.swagger.v3.oas.annotations.parameters.RequestBody(
                    required = true,
                    description = "Update an author request",
                    content = @Content(
                            mediaType = MediaType.APPLICATION_JSON_VALUE,
                            schema = @Schema(implementation = AuthorDTO.class))),
            responses = @ApiResponse(responseCode = "200"))
    public Mono<Void> updateAuthor(
            @PathVariable final String authorId,
            @RequestBody @Valid final AuthorDTO authorDTO) {

        return authorService.update(authorId, authorDTO);
    }

    @DeleteMapping("/{authorId}")
    @Operation(
            method = "delete",
            summary = "Delete an author",
            description = "Delete an author",
            operationId = "delete",
            parameters = @Parameter(
                    name = "authorId",
                    description = "The author id",
                    in = ParameterIn.PATH,
                    required = true),
            responses = @ApiResponse(responseCode = "204"))
    public Mono<Void> deleteAuthor(@PathVariable final String authorId) {
        return authorService.delete(authorId);
    }

}
