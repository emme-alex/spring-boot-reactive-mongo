package it.matteo.spring_boot_reactive_mongo.service;

import it.matteo.spring_boot_reactive_mongo.domain.Author;
import it.matteo.spring_boot_reactive_mongo.model.AuthorDTO;
import it.matteo.spring_boot_reactive_mongo.repos.AuthorRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
@RequiredArgsConstructor
public class AuthorService {

    private final AuthorRepository authorRepository;

    public Flux<AuthorDTO> findAll() {
        return authorRepository.findAll(Sort.by("authorId")).
                map(author -> mapToDTO(author, new AuthorDTO()));
    }

    public Mono<AuthorDTO> get(final String authorId) {
        return authorRepository.findById(authorId)
                .map(author -> mapToDTO(author, new AuthorDTO()))
                .onErrorResume(throwable ->
                        Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND, throwable.getMessage())));
    }

    public Mono<String> create(final AuthorDTO authorDTO) {
        final Author author = new Author();
        mapToEntity(authorDTO, author);
        return authorRepository.save(author)
                .map(Author::getAuthorId);
    }

    public Mono<Void> update(final String authorId, final AuthorDTO authorDTO) {
        return authorRepository.findById(authorId)
                .onErrorResume(throwable ->
                        Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND, throwable.getMessage())))
                .map(author -> mapToEntity(authorDTO, author))
                .flatMap(authorRepository::save)
                .then();
    }

    public Mono<Void> delete(final String authorId) {
        return authorRepository.deleteById(authorId);
    }

    private AuthorDTO mapToDTO(final Author author, final AuthorDTO authorDTO) {
        authorDTO.setAuthorId(author.getAuthorId());
        authorDTO.setAuthorName(author.getAuthorName());
        return authorDTO;
    }

    private Author mapToEntity(final AuthorDTO authorDTO, final Author author) {
        author.setAuthorName(authorDTO.getAuthorName());
        return author;
    }

}
