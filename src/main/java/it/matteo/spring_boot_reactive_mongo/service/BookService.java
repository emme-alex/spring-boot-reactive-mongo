package it.matteo.spring_boot_reactive_mongo.service;

import it.matteo.spring_boot_reactive_mongo.domain.Author;
import it.matteo.spring_boot_reactive_mongo.domain.Book;
import it.matteo.spring_boot_reactive_mongo.model.BookDTO;
import it.matteo.spring_boot_reactive_mongo.repos.AuthorRepository;
import it.matteo.spring_boot_reactive_mongo.repos.BookRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;

@Service
@Slf4j
@RequiredArgsConstructor
public class BookService {

    private final BookRepository bookRepository;
    private final AuthorRepository authorRepository;

    public Flux<BookDTO> findAll() {
        return bookRepository.findAll(Sort.by("bookId"))
                .map(book -> mapToDTO(book, new BookDTO()));
    }

    public Mono<BookDTO> get(final String bookId) {
        return bookRepository.findById(bookId)
                .map(book -> mapToDTO(book, new BookDTO()))
                .onErrorResume(throwable -> Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND)));
    }

    public Mono<String> create(final BookDTO bookDTO) {
        return mapToEntity(bookDTO, new Book())
                .flatMap(bookRepository::save)
                .map(Book::getBookId)
                .doOnError(throwable -> log.error("While creating Book", throwable));
    }

    public Mono<Void> update(final String bookId, final BookDTO bookDTO) {
        return bookRepository.findById(bookId)
                .onErrorResume(throwable -> Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND)))
                .flatMap(book -> mapToEntity(bookDTO, book))
                .map(bookRepository::save)
                .then();
    }

    public Mono<Void> delete(final String bookId) {
        return bookRepository.deleteById(bookId);
    }

    private BookDTO mapToDTO(final Book book, final BookDTO bookDTO) {
        bookDTO.setBookId(book.getBookId());
        bookDTO.setTitle(book.getTitle());
        bookDTO.setPages(book.getPages());
        bookDTO.setPublicationDate(book.getPublicationDate());
        bookDTO.setAuthors(book.getAuthors() == null ? new ArrayList<>() : new ArrayList<>(book.getAuthors()));
        return bookDTO;
    }

    private Mono<Book> mapToEntity(final BookDTO bookDTO, final Book book) {
        book.setTitle(bookDTO.getTitle());
        book.setPages(bookDTO.getPages());
        book.setPublicationDate(bookDTO.getPublicationDate());

        return authorRepository.findAllById(
                        bookDTO.getAuthors() == null ? Collections.emptyList() : bookDTO.getAuthors())
                .map(Author::getAuthorId)
                .collectList()
                .flatMap(bookAuthors -> {
                    if (bookAuthors.size() != (bookDTO.getAuthors() == null ? 0 : bookDTO.getAuthors().size())) {
                        return Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND, "one of bookAuthors not found"));
                    }
                    book.setAuthors(new HashSet<>(bookAuthors));
                    return Mono.just(book);
                });
    }

}
