package it.matteo.spring_boot_reactive_mongo.repos;

import it.matteo.spring_boot_reactive_mongo.domain.Author;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

public interface AuthorRepository extends ReactiveMongoRepository<Author, String> {
}
