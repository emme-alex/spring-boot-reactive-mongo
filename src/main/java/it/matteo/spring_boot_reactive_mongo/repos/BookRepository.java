package it.matteo.spring_boot_reactive_mongo.repos;

import it.matteo.spring_boot_reactive_mongo.domain.Book;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

public interface BookRepository extends ReactiveMongoRepository<Book, String> {
}
