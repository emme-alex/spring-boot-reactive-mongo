package it.matteo.spring_boot_reactive_mongo.repos;

import it.matteo.spring_boot_reactive_mongo.domain.Author;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.reactivestreams.Publisher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.autoconfigure.data.mongo.AutoConfigureDataMongo;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Example;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import reactor.test.StepVerifier;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

@ActiveProfiles("test")
@AutoConfigureDataMongo
@SpringBootTest(
        properties = "de.flapdoodle.mongodb.embedded.version=5.0.5"
)
@EnableAutoConfiguration
@DirtiesContext
public class AuthorEntityRepoTest extends TestUtils {

    @DisplayName("Author repository not null")
    @Test
    void testNotNull(@Autowired final AuthorRepository authorRepository) {
        assertThat(authorRepository).isNotNull();
    }

    @DisplayName("Save an author")
    @Test
    public void shouldSaveSingleAuthor(@Autowired final AuthorRepository authorRepository) {
        final Author author = Author.builder()
                .authorName("Author test")
                .build();
        final Publisher<Author> setup =
                authorRepository.deleteAll().thenMany(authorRepository.save(author));
        StepVerifier
                .create(setup)
                .expectNextCount(1)
                .verifyComplete();
    }

    @DisplayName("Update an author")
    @Test
    public void shouldUpdateSingleAuthor(@Autowired final AuthorRepository authorRepository) {
        final String newName = "author updated name";
        final Publisher<Author> setup =
                authorRepository.deleteAll()
                        .thenMany(
                                authorRepository.save(
                                        createTestAuthor("author name"))
                        )
                        .flatMap(createdAuthor -> {
                            createdAuthor.setAuthorName(newName);
                            return authorRepository.save(createdAuthor);
                        });
        StepVerifier
                .create(setup)
                .assertNext(updatedAuthor -> assertEquals(newName, updatedAuthor.getAuthorName()))
                .verifyComplete();
    }

    @DisplayName("Delete an author")
    @Test
    public void shouldDeleteSingleAuthor(@Autowired final AuthorRepository authorRepository) {
        final Author author = createTestAuthor("test author name");
        final Publisher<Author> setup =
                authorRepository.deleteAll()
                        .thenMany(
                                authorRepository.save(author)
                        )
                        .flatMap(authorRepository::delete)
                        .flatMap(unused -> authorRepository.findOne(Example.of(author)));
        StepVerifier
                .create(setup)
                // it will return Mono.empty() so just "verifyComplete()"
                .verifyComplete();
    }

    @DisplayName("Retrieve an author")
    @Test
    public void shouldRetrieveSingleAuthor(@Autowired final AuthorRepository authorRepository) {
        final String name = "test name";
        final Publisher<Author> setup =
                authorRepository.deleteAll()
                        .thenMany(
                                authorRepository.save(createTestAuthor(name))
                        )
                        .flatMap(createdAuthor -> {
                            final Author newAuthorToFind = Author.builder()
                                    .authorName(name)
                                    .build();
                            return authorRepository.findOne(Example.of(newAuthorToFind));
                        });
        StepVerifier
                .create(setup)
                .assertNext(foundAuthor -> assertEquals(name, foundAuthor.getAuthorName()))
                .verifyComplete();
    }

}
