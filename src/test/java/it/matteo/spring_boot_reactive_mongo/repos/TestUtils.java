package it.matteo.spring_boot_reactive_mongo.repos;

import it.matteo.spring_boot_reactive_mongo.domain.Author;
import it.matteo.spring_boot_reactive_mongo.domain.Book;

import java.time.LocalDateTime;

public abstract class TestUtils {

    protected Book createTestBook(final String title) {
        return Book.builder()
                .title(title)
                .pages((long) 200)
                .publicationDate(LocalDateTime.of(2000, 4, 3, 0, 0))
                .build();
    }

    protected Author createTestAuthor(final String name) {
        return Author.builder()
                .authorName(name)
                .build();
    }

}
