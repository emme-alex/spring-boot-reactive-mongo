package it.matteo.spring_boot_reactive_mongo.repos;

import it.matteo.spring_boot_reactive_mongo.domain.Book;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.reactivestreams.Publisher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.autoconfigure.data.mongo.AutoConfigureDataMongo;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Example;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import reactor.test.StepVerifier;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

@ActiveProfiles("test")
@AutoConfigureDataMongo
@SpringBootTest(
        properties = "de.flapdoodle.mongodb.embedded.version=5.0.5"
)
@EnableAutoConfiguration
@DirtiesContext
public class BookEntityRepoTest extends TestUtils {

    @DisplayName("Book repository not null")
    @Test
    void testNotNull(@Autowired final BookRepository bookRepository) {
        assertThat(bookRepository).isNotNull();
    }

    @DisplayName("Save a book")
    @Test
    public void shouldSaveSingleBook(@Autowired final BookRepository bookRepository) {
        final Publisher<Book> setup =
                bookRepository.deleteAll()
                        .thenMany(
                                bookRepository.save(
                                        createTestBook("test title"))
                        );
        StepVerifier
                .create(setup)
                .expectNextCount(1)
                .verifyComplete();
    }

    @DisplayName("Update a book")
    @Test
    public void shouldUpdateSingleBook(@Autowired final BookRepository bookRepository) {
        final String newTitle = "test updated title";
        final Publisher<Book> setup =
                bookRepository.deleteAll()
                        .thenMany(
                                bookRepository.save(
                                        createTestBook("test title"))
                        )
                        .flatMap(createdBook -> {
                            createdBook.setTitle(newTitle);
                            return bookRepository.save(createdBook);
                        });
        StepVerifier
                .create(setup)
                .assertNext(updatedBook -> assertEquals(newTitle, updatedBook.getTitle()))
                .verifyComplete();
    }

    @DisplayName("Delete a book")
    @Test
    public void shouldDeleteSingleBook(@Autowired final BookRepository bookRepository) {
        final Book book = createTestBook("test title");
        final Publisher<Book> setup =
                bookRepository.deleteAll()
                        .thenMany(
                                bookRepository.save(book)
                        )
                        .flatMap(bookRepository::delete)
                        .flatMap(unused -> bookRepository.findOne(Example.of(book)));
        StepVerifier
                .create(setup)
                // it will return Mono.empty() so just "verifyComplete()"
                .verifyComplete();
    }

    @DisplayName("Retrieve a book")
    @Test
    public void shouldRetrieveSingleBook(@Autowired final BookRepository bookRepository) {
        final String title = "test title";
        final Publisher<Book> setup =
                bookRepository.deleteAll()
                        .thenMany(
                                bookRepository.save(createTestBook(title))
                        )
                        .flatMap(createdBook -> {
                            final Book newBookToFind = Book.builder()
                                    .title(title)
                                    .build();
                            return bookRepository.findOne(Example.of(newBookToFind));
                        });
        StepVerifier
                .create(setup)
                .assertNext(foundBook -> assertEquals(title, foundBook.getTitle()))
                .verifyComplete();
    }

}
