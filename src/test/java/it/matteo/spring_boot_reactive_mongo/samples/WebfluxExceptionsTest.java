package it.matteo.spring_boot_reactive_mongo.samples;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Slf4j
public class WebfluxExceptionsTest {

    @Test
    public void onErrorReturnDirectly_Mono() {
        Mono.just(2)
                .map(i -> i / 0) // will produce ArithmeticException
                .onErrorReturn(4)
                .subscribe(num -> log.info("Number: {}", num));
    }

    @Test
    public void onErrorReturnIfArithmeticException_Mono() {
        Mono.just(2)
                .map(i -> i / 0) // will produce ArithmeticException
                .onErrorReturn(ArithmeticException.class, 4)
                .subscribe(num -> log.info("Number: {}", num));
    }

    @Test
    public void onErrorReturnIfPredicatePasses_Mono() {
        Mono.just(2)
                .map(i -> i / 0) // will produce ArithmeticException
                .onErrorReturn(error -> error instanceof ArithmeticException, 4)
                .subscribe(num -> log.info("Number: {}", num));
    }

    @Test
    public void onErrorResume_Mono() {
        Mono.just(2)
                .map(i -> i / 0) // will produce ArithmeticException
                .onErrorResume(error -> Mono.just(1111))
                .subscribe(num -> log.info("Number: {}", num));
    }

    @Test
    public void onErrorResumeIfArithmeticException_Mono() {
        Mono.just(2)
                .map(i -> i / 0) // will produce ArithmeticException
                .onErrorResume(
                        ArithmeticException.class,
                        error -> Mono.just(4)
                )
                .subscribe(num -> log.info("Number: {}", num));
    }

    @Test
    public void onErrorResumeIfPredicatePasses_Mono() {
        Mono.just(2)
                .map(i -> i / 0) // will produce ArithmeticException
                .onErrorResume(
                        error -> error instanceof ArithmeticException,
                        error -> Mono.just(4)
                )
                .subscribe(num -> log.info("Number: {}", num));
    }

    @Test
    public void onErrorContinue_Mono() {
        Mono.just(2)
                .map(i -> i / 0) // will produce ArithmeticException
                .onErrorContinue((error, obj) -> log.info("error:[{}], obj:[{}]", error, obj))
                .subscribe(num -> log.info("Number: {}", num));
    }

    @Test
    public void onErrorContinueIfArithmeticException_Mono() {
        Mono.just(2)
                .map(i -> i / 0) // will produce ArithmeticException
                .onErrorContinue(
                        ArithmeticException.class,
                        (error, obj) -> log.info("error:[{}], obj:[{}]", error, obj)
                )
                .subscribe(num -> log.info("Number: {}", num));
    }

    @Test
    public void onErrorContinueIfPredicatePasses_Mono() {
        Mono.just(2)
                .map(i -> i / 0) // will produce ArithmeticException
                .onErrorContinue(
                        error -> error instanceof ArithmeticException,
                        (error, obj) -> log.info("error:[{}], obj:[{}]", error, obj)
                )
                .subscribe(num -> log.info("Number: {}", num));
    }

    @Test
    public void doOnError_Mono() {
        Mono.just(2)
                .map(i -> i / 0) // will produce ArithmeticException
                .doOnError(error -> log.info("caught error"))
                .subscribe(num -> log.info("Number: {}", num));
    }

    @Test
    public void doOnErrorIfArithmeticException_Mono() {
        Mono.just(2)
                .map(i -> i / 0) // will produce ArithmeticException
                .doOnError(
                        ArithmeticException.class,
                        error -> log.info("caught error")
                )
                .subscribe(num -> log.info("Number: {}", num));
    }

    @Test
    public void doOnErrorIfPredicatePasses_Mono() {
        Mono.just(2)
                .map(i -> i / 0) // will produce ArithmeticException
                .doOnError(
                        error -> error instanceof ArithmeticException,
                        error -> log.info("caught error")
                )
                .subscribe(num -> log.info("Number: {}", num));
    }

    @Test
    public void OnErrorMap_Mono() {
        Mono.just(2)
                .map(i -> i / 0) // will produce ArithmeticException
                .onErrorMap(error -> new RuntimeException("SomeMathException"))
                .subscribe(num -> log.info("Number: {}", num));
    }

    @Test
    public void OnErrorMapIfArithmeticException_Mono() {
        Mono.just(2)
                .map(i -> i / 0) // will produce ArithmeticException
                .onErrorMap(
                        ArithmeticException.class,
                        error -> new RuntimeException("SomeMathException")
                )
                .subscribe(num -> log.info("Number: {}", num));
    }

    @Test
    public void OnErrorMapIfPredicatePasses_Mono() {
        Mono.just(2)
                .map(i -> i / 0) // will produce ArithmeticException
                .onErrorMap(
                        error -> error instanceof ArithmeticException,
                        error -> new RuntimeException("SomeMathException")
                )
                .subscribe(num -> log.info("Number: {}", num));
    }

    @DisplayName("Per capire bene come funge: doOnError() e onErrorResume()")
    @Test
    public void doOnError_Flux() {
        Flux.just(5, 2, 4, 5)
                .subscribe(num -> log.info("Number (no errors): {}", num));
        log.info("---");

        Flux.just(5, 2, 4, 5)
                .map(i -> i / 0) // will produce ArithmeticException
                .doOnError(
                        error -> error instanceof ArithmeticException,
                        error -> log.info("caught error")
                )
                .subscribe(num -> log.info("Number: {}", num));
        log.info("---");

        Flux.just(5, 2, 4, 5)
                .map(i -> i / 0) // will produce ArithmeticException
                .doOnError(
                        error -> error instanceof ArithmeticException,
                        error -> log.info("caught error")
                )
                .onErrorResume(throwable -> Mono.just(1111))
                .doOnNext(num -> log.info("Number onNext: {}", num))
                .doOnEach(signal -> log.info("Signal onEach: {}", signal))
                .subscribe(num -> log.info("Number: {}", num));
    }

}
