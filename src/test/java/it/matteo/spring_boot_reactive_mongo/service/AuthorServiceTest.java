package it.matteo.spring_boot_reactive_mongo.service;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.autoconfigure.data.mongo.AutoConfigureDataMongo;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import reactor.test.StepVerifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@ActiveProfiles("test")
@AutoConfigureDataMongo
@SpringBootTest(
        properties = "de.flapdoodle.mongodb.embedded.version=5.0.5"
)
@EnableAutoConfiguration
@DirtiesContext
public class AuthorServiceTest {

    @DisplayName("GET all authors")
    @Test
    void testGet(@Autowired final AuthorService authorService) {
        StepVerifier.create(authorService.findAll().collectList())
                .assertNext(list -> {
                    assertNotNull(list);
                    assertEquals(0, list.size());
                })
                .verifyComplete();
    }

}
