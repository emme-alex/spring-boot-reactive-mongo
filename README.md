# Spring Boot 3.1.x + Spring Webflux + MongoDB

This app was created with [Bootify.io](https://bootify.io/docs/).

## Requirements

- Java JDK >= 21
- [Docker](https://docs.docker.com/get-docker/) installed

## Build

The application can be built using the following command:

```shell
./mvnw clean package
```

The application can then be started with the following commands:

### Profile `dev`

No embedded _Mongo_, need to specify a valid URL in 
[application.properties](src/main/resources/application.properties) 

```shell
./mvnw -Pdev
```

### Profile `debug`

Like `dev`, plus _debug_ enabled

```shell
./mvnw -Pdebug
```

### Profile `dev-docker`

Like `dev`, plus embedded _Mongo_ via _Docker_

```shell
./mvnw -Pdev-docker
```

### Profile `tests`

To run all tests

```shell
./mvnw -Ptests
```

---

## API Docs

- http://localhost:8080/swagger-ui.html
- http://localhost:8080/swagger-ui.html?configUrl=/v3/api-docs/swagger-config
- http://localhost:8080/webjars/swagger-ui/index.html

---

## GraphQL

- http://localhost:8080/graphiql

---

## MongoDB

```
mongodb://myuser:test123@localhost:27018/spring-boot-reactive-mongo
```

Connect to the Docker `mongodb` container:

```shell
docker exec -it mongodb bash
```

Get DB info:

```shell
# connect to mongodb server via CLI, + authentication
mongosh -u myuser -p test123

# switch to DB
use spring-boot-reactive-mongo

# list DBs
show dbs
```

---